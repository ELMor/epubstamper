
package epub.stamper;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class Recurse {

	File raiz;

	public static void main(String args[]) {
		if (args.length != 1) {
			System.out.println("Must specify dir.");
			System.exit(0);
		}
		File root = new File(args[0]);
		if (!root.isDirectory()) {
			System.out.println((new StringBuilder(String.valueOf(root
					.getAbsolutePath()))).append(" must be a directory")
					.toString());
			System.exit(0);
		}
		Recurse r = new Recurse(root);
		r.proceed();
	}

	public Recurse(File init) {
		raiz = null;
		raiz = init;
	}

	public void proceed() {
		recurse(raiz);
	}

	private void recurse(File dir) {
		File lista[] = dir.listFiles();
		File afile[];
		int j = (afile = lista).length;
		for (int i = 0; i < j; i++) {
			File f = afile[i];
			if (f.isDirectory())
				recurse(f);
			else if (f.getName().toLowerCase().endsWith(".epub")) {
				try {
					check(f);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}

	private void o(String s) {
		System.out.print(s);
	}

	public static String toUnhandText(String app) {
		app = app.replace('\f', ' ');
		app = app.replace("\u2019", "'");
		app = app.replace("\u2018", "'");
		app = app.replace("\"", "'");
		app = app.replace("\u201C", "'");
		app = app.replace("\u201D", "'");
		app = app.replace("\u2014", "-");
		app = app.replace("\u2026", "...");
		app = app.replace("\306", "AE");
		app = app.replace("\341", "a");
		app = app.replace("\351", "e");
		app = app.replace("\355", "i");
		app = app.replace("\363", "o");
		app = app.replace("\372", "u");
		app = app.replace("\301", "A");
		app = app.replace("\311", "E");
		app = app.replace("\315", "I");
		app = app.replace("\323", "O");
		app = app.replace("\332", "U");
		app = app.replace("\361", "n");
		app = app.replace("\321", "N");
		app = app.replace("\t", " ");
		app = app.replace("\n", " ");
		byte n[] = app.getBytes();
		for (int i = 0; i < n.length; i++)
			if (n[i] < 0)
				n[i] = 32;

		app = new String(n);
		return app;
	}

	private void check(File f) throws Exception {
		ZipInputStream zis;
		Hashtable<String, ByteArrayOutputStream> zfc;
		zis = new ZipInputStream(new FileInputStream(f));
		zfc = new Hashtable<String, ByteArrayOutputStream>();
		for (ZipEntry ze = zis.getNextEntry(); 
				ze != null; 
				ze = zis.getNextEntry())
			if (ze.getName().equals("mimetype")) {
				getBAOS(zis);
			} else {
				ByteArrayOutputStream baos = getBAOS(zis);
				zfc.put(ze.getName(), baos);
			}
		zis.close();
		int xhtmlfileNumber = 1;
		for (Enumeration<String> fne = zfc.keys(); fne.hasMoreElements();) {
			String fn = (String) fne.nextElement();
			if (fn.startsWith("OEBPS/") && fn.toLowerCase().endsWith(".css")) {
				checkCSSStyles(f, zfc, fn);
			} else if (fn.startsWith("OEBPS/")
					&& fn.toLowerCase().endsWith(
							"-" + xhtmlfileNumber + ".xhtml")) {
				checkSpanDivs(f, zfc, fn);
				xhtmlfileNumber++;
			}
		}
	}

	private void checkSpanDivs(File f,
			Hashtable<String, ByteArrayOutputStream> zfc, String fn)
			throws IOException, FileNotFoundException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		StringBuffer cnt = new StringBuffer(((ByteArrayOutputStream) zfc.get(fn)).toString());
		boolean modified=false;
		
		int openDivNdx=cnt.indexOf("<div class"),closeDivNdx;
		int numdivs=0;
		while(openDivNdx>0 && (closeDivNdx=cnt.indexOf("</div>",openDivNdx))>0){
			numdivs++;
			int lenOfInitialText=closeDivNdx-openDivNdx;
			String className=cnt.substring(openDivNdx+12,openDivNdx+17);
			int firstOpenDivNdx=openDivNdx;
			openDivNdx=cnt.indexOf("<div class",closeDivNdx);
			if(lenOfInitialText>70 && openDivNdx-closeDivNdx<=10){ //muy seguidos
				if(className.equals(cnt.substring(openDivNdx+12,openDivNdx+17))){//misma clase
					int lastCharNdx=-1;
					char chr=cnt.charAt(closeDivNdx+lastCharNdx);
					while(" \r\n\t".indexOf(chr)>=0)
						chr=cnt.charAt(closeDivNdx+(--lastCharNdx));
					if(chr=='>')//debe ser </span>
						chr=cnt.charAt(closeDivNdx+lastCharNdx-7);
					if(notEndingCharForParagrapghh(chr)){ 
						//Unimos si podemos
						int to1=cnt.indexOf(">", openDivNdx);
						cnt.delete(closeDivNdx, to1+1);
						cnt.insert(closeDivNdx, " ");
						modified=true;
						openDivNdx=firstOpenDivNdx;
					}
					
				}
			}
			//Primero buscamos 
		}
		
		if (modified) {
			modifyFile(f, zfc, fn, baos, cnt);
		}
	}
	
	private boolean notEndingCharForParagrapghh(char chr){
		return  	(chr>='A'&&chr<='Z')
		|| 	(chr>='a'&&chr<='z')
		||  chr==' '
		|| 	(chr>='0'&&chr<='9')
		||  "����������".indexOf(chr)>=0
		||  ",;:-��()".indexOf(chr)>=0
		;
	}

	private void checkCSSStyles(File f,
			Hashtable<String, ByteArrayOutputStream> zfc, String fn)
			throws IOException, FileNotFoundException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		StringBuffer cnt = new StringBuffer(((ByteArrayOutputStream) zfc.get(fn)).toString());

		boolean modified;
		modified = removeStyle(cnt);
		modified |= changeStyle(cnt, "text-indent:1.0em", "text-indent:2.0em");
		modified |= addStyle(cnt, "{ text-align:justify;", "margin-bottom:1em ");
		if (modified) {
			modifyFile(f, zfc, fn, baos, cnt);
		}
	}

	private void modifyFile(File f,
			Hashtable<String, ByteArrayOutputStream> zfc, String fn,
			ByteArrayOutputStream baos, StringBuffer cnt) throws IOException,
			FileNotFoundException {
		System.out.println("Changing " + f.getName());
		baos.write(cnt.toString().getBytes());
		zfc.put(fn, baos);
		changeFile(f, zfc);
	}

	private boolean addStyle(StringBuffer cnt, String styleName, String style) {
		int ndx = cnt.indexOf(styleName);
		return addStyleRecurse(cnt, styleName, style, ndx, false);
	}

	private boolean addStyleRecurse(StringBuffer cnt, String styleName, String style, int ndx, boolean modify) {
		if (ndx < 0)
			return modify;
		int ndx2 = cnt.indexOf("}", ndx);
		if (ndx2 < 0)
			return modify;
		int n=cnt.indexOf(style,ndx);
		if(!(n>0 && n<ndx2)){
			cnt.insert(ndx2 - 1, "; " + style);
			modify=true;
		}
		if((ndx = cnt.indexOf(styleName,ndx+1))>0)
			return addStyleRecurse(cnt, styleName, style, ndx, modify);
		return modify;
	}

	private boolean removeStyle(StringBuffer cnt) {
		int st1=0;
		boolean modified = false;
		while ((st1 = cnt.indexOf("word-spacing",st1)) >= 0) {
			int st2 = cnt.indexOf("em", st1 + 10);
			if (st2 >= 0) {
				cnt.delete(st1, st2 + 2);
				modified = true;
			}else{
				st1++;
			}
		}
		return modified;
	}

	private boolean changeStyle(StringBuffer cnt, String srch, String repl) {
		int ndx;
		boolean mod = false;
		while ((ndx = cnt.indexOf(srch)) >= 0) {
			cnt.replace(ndx, ndx + srch.length(), repl);
			mod = true;
		}
		return mod;
	}

	private ByteArrayOutputStream getBAOS(ZipInputStream zis)
			throws IOException {
		byte buf[] = new byte[2048];
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		int lread;
		while ((lread = zis.read(buf)) >= 0)
			baos.write(buf, 0, lread);
		return baos;
	}

	private void changeFile(File f, Hashtable<String, ByteArrayOutputStream> zfc)
			throws FileNotFoundException, IOException {
		File f2 = new File((new StringBuilder(String.valueOf(f
				.getCanonicalPath()))).append("-tmp").toString());
		ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(f2));
		ZipEntry ze = new ZipEntry("mimetype");
		ze.setMethod(0);
		ze.setSize(20L);
		ze.setCrc(0x2cab616fL);
		zos.putNextEntry(ze);
		zos.write("application/epub+zip".getBytes());
		String n;
		for (Enumeration<String> fn = zfc.keys(); fn.hasMoreElements(); ((ByteArrayOutputStream) zfc
				.get(n)).writeTo(zos)) {
			n = (String) fn.nextElement();
			zos.putNextEntry(new ZipEntry(n));
		}

		zos.close();
		f.delete();
		f2.renameTo(f);
	}
}
